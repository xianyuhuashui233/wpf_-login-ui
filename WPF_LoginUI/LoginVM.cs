﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPF_LoginUI
{
    public class LoginVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChenged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        
        private LoginModel loginM = new LoginModel();

        public LoginModel LoginM
        {
            get { 
                if (loginM is null) 
                    loginM = new LoginModel();  
                return loginM; 
            }
            set { 
                loginM = value;
                RaisePropertyChenged(nameof(LoginM)); 
            }
        }

        private LoginModel _LoginM = new LoginModel();

        public string UserName
        {
            get { return _LoginM.UserName; }
            set { _LoginM.UserName = value;
                RaisePropertyChenged(nameof(UserName));
            }
        }

        public string Password
        {
            get { return _LoginM.Password; }
            set
            {
                _LoginM.Password = value;
                RaisePropertyChenged(nameof(Password));
            }
        }

        void LoginFunc()
        {
            if (UserName == "admin" && Password == "123456")
            {
                MessageBox.Show("OK");
                Index index = new Index();
                //this.Hide();
                index.Show();
            }
            else
            {
                MessageBox.Show("用户名或密码错误");
                UserName = "";
                Password = "";
            }
        }
    }
}
